import * as C from "./crypto.min.js";


(async ()=>{
  const data = "abcdefg123456789";
  const AES_key_1 = await C.randomKey(32);
  const AES_key_2 = "some_random_text";

  //some string operations
  var hash = await C.sha256(data);
  console.log("sha256: ", await C.decode(hash));

  var btoa =  C.btoa(data);
  var atob =  C.atob(data);
  console.log("btoa: ", btoa, " atob: ", atob);

  //AES
  var encryptedAES = await C.AES.encrypt(data,AES_key_1);
  var decryptedAES = await C.AES.decrypt(encryptedAES,AES_key_1);

  console.log("AES | Encrypted: ", encryptedAES," Decrypted: ", decryptedAES);

  //RSA
  const encrKey = await C.RSA.generateEncryptionKey();
  const signKey = await C.RSA.generateSigningKey();

  //RSA encrypt
  var encryptedRSA = await C.RSA.encrypt(data,encrKey.publicKey);
  var decryptedRSA = await C.RSA.decrypt(encryptedRSA,encrKey.privateKey);

  console.log("RSA | Encrypted: ", encryptedRSA," Decrypted: ", decryptedRSA);

  //RSA signing
  var certificate = await C.RSA.sign(data,encrKey.privateKey);
  var isvaild = await C.RSA.verify(data,encryptedRSA,encrKey.publicKey);

  console.log("RSA Signing | Cerificate: ", certificate," Is Valid?: ", isvaild ? "valid" : "invalid");

  //communicate (RSA and AES for large Data)
  ///Only the Server has a certificate
  var commEncrypted = await C.communicate.encrypt(data,encrKey.publicKey);
  //randKey for AES: commEncrypted.key
  var commDecrypted = await C.communicate.decrypt(commEncrypted.send,encrKey.privateKey);
  //randKey for AES: commDecrypted.key
  //If an error occures -> commDecrypted == null
  console.log("Communicate | Encrypted: ",commEncrypted.send, " Decrypted: ",commDecrypted.data);

})();
